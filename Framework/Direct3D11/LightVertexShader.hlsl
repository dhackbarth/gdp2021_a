cbuffer MatrixBuffer
{
    matrix worldViewProjectionMatrix;
    matrix worldMatrix;
    float3 cameraWorldPosition;
};

struct VertexInput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 positionWorld : POSITION1;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
    float3 view : TEXCOORD1;
};

VertexOutput main(VertexInput INPUT)
{
    VertexOutput OUTPUT;
    
    OUTPUT.position = mul(float4(INPUT.position, 1.0f), worldViewProjectionMatrix);
    OUTPUT.positionWorld = mul(float4(INPUT.position, 1.0f), worldMatrix).xyz;
    OUTPUT.normal = mul(INPUT.normal, (float3x3) worldMatrix);
    OUTPUT.uv = INPUT.uv * float2(1.0f, 1.0f) + float2(0.0f, 0.0f); // uv * tiling + offset
    OUTPUT.view = cameraWorldPosition - OUTPUT.positionWorld;
    
    return OUTPUT;
}