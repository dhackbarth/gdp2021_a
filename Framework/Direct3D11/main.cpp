#include <Windows.h>
#include <random>
#include "Window.h"
#include "D3D.h"
#include "Mesh.h"
#include "Camera.h"
#include "Time.h"
#include "Material.h"
#include "Light.h"
#include "Utils.h"

int WINAPI WinMain(
	HINSTANCE hInstance, // handle to our application instance
	HINSTANCE hPrevInstance, // deprecated
	PSTR szCmdLine, // command line
	INT nCmdShow // how to present the window
)
{
	INT error = 0;
	INT width = 1024;
	INT height = 768;
	BOOL isFullscreen = false;

	// 1. create a window
	Window wnd = {};
	error = wnd.init(hInstance, width, height, nCmdShow);
	CheckError(error);

	// 2. create Direct3D connection
	D3D d3d = {};
	error = d3d.init(wnd.getWindowHandle(), width, height, isFullscreen);
	CheckError(error);

	// 3. create mesh
	Mesh mesh = {};
	error = mesh.init(d3d.getDevice());
	mesh.setPosition({ 0.0f, 0.0f, 0.0f });
	mesh.setRotation({ 70.0f, 0.0f, 0.0f });
	CheckError(error);
	Mesh mesh2 = {};
	error = mesh2.init(d3d.getDevice());
	CheckError(error);

	// 4. create camera
	Camera camera = {};
	error = camera.init(width, height);
	CheckError(error);

	// 5. create time
	Time time = {};
	error = time.init();
	CheckError(error);

	// 6. create material
	Material material = {};
	error = material.init(d3d.getDevice(), TEXT("wall.jpg"));
	CheckError(error);

	// 7. create light
	Light light = {};
	Light::LightData lightData = { { 0.0f, 1.0f, 0.0f }, Light::LightType::Point };
	//lightData.lightDirection = { -1.0f, -1.0f, 1.0f };
	lightData.lightIntensity = 1.0f;
	lightData.ambientLight = { 0.2f, 0.2f, 0.2f, 1.0f };
	lightData.diffuseLight = { 0.6f, 0.6f, 0.6f, 1.0f };
	lightData.specularLight = { 1.0f, 1.0f, 1.0f, 1.0f };
	//lightData.emissionLight = { 0.0f, 0.5f, 0.0f, 1.0f };
	error = light.init(d3d.getDevice(), lightData);
	CheckError(error);

	// run phase
	while (wnd.run())
	{
		// update objects
		time.update();
		camera.update(time.getDeltaTime());
		mesh.update(time.getDeltaTime());
		mesh2.update(time.getDeltaTime());

		// draw objects
		d3d.beginScene(0.0f, 0.0f, 0.0f);

		// mesh 2
		material.render(d3d.getDeviceContext(), mesh2.getWorldMatrix(), camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition());
		mesh2.render(d3d.getDeviceContext());

		// mesh 1
		material.render(d3d.getDeviceContext(), mesh.getWorldMatrix(), camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition());
		light.render(d3d.getDeviceContext());
		mesh.render(d3d.getDeviceContext());

		d3d.endScene();
	}

	// tidy up
	light.deInit();
	material.deInit();
	time.deInit();
	camera.deInit();
	mesh.deInit();
	d3d.deInit();
	wnd.deInit();

	return 0;
}