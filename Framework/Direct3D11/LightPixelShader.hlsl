Texture2D MainTexture;
sampler MainSampler;

cbuffer LightData
{
    //float3 lightDirection; // only for directional light
    float4 lightWorldPosition; // for different light sources, w -> 0.0f - directional light, 1.0f - point light
    float4 ambientLight;
    float4 diffuseLight;
    float4 specularLight;
    float4 emissionLight;
	float lightIntensity;
};

struct PixelInput
{
    float4 position : SV_POSITION;
    float3 positionWorld : POSITION1;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float3 view : TEXCOORD1;
};

float4 main(PixelInput INPUT) : SV_TARGET
{
    float4 textureColor = MainTexture.Sample(MainSampler, INPUT.uv);
    float4 ambientColor = ambientLight;
    float4 diffuseColor = 0;
    float4 specularColor = 0;
    float4 emissionColor = emissionLight;
    
    // light vector
    float3 normalVector = normalize(INPUT.normal);
    //float3 lightVector = normalize(lightDirection); // only for directional light
    float3 lightVector = lightWorldPosition.xyz - INPUT.positionWorld * lightWorldPosition.w;
    float lightLength = length(lightVector);
    lightVector /= lightLength;
    float attenuation = 1 / (1.0f + (0.2f * lightLength + 0.1f * lightLength * lightLength) * lightWorldPosition.w);
    
    // diffuse color
    float diffuse = max(dot(normalVector, lightVector), 0);
    diffuseColor = diffuseLight * (diffuse * lightIntensity * attenuation);
    
    // specular color with Phong light model
    //float3 reflectVector = reflect(-lightVector, normalVector);
    //float3 viewVector = normalize(INPUT.view);
    //float phongExponent = 512.0f;
    //float specular = pow(max(dot(reflectVector, viewVector), 0), phongExponent);
    //specularColor = specularLight * (specular * lightIntensity * attenuation);
    
    // specular color with BlinnPhong light model
    float3 viewVector = normalize(INPUT.view);
    float3 halfVector = normalize(lightVector + viewVector);
    float phongExponent = 1024.0f;
    float specular = pow(max(dot(normalVector, halfVector), 0), phongExponent);
    specularColor = specularLight * (specular * lightIntensity * attenuation);
    
    // texture * (ambient + diffuse) + specular + emission
    return saturate(textureColor * saturate(ambientColor + diffuseColor) + specularColor + emissionColor);
}