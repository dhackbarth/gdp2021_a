#pragma once
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

class Light
{
public:
	enum LightType
	{
		Directional = 0,
		Point = 1
	};

	struct LightData
	{
		//XMFLOAT3 lightDirection; // only for directional light
		XMFLOAT4 lightWorldPosition; // for different light sources, w -> 0.0f - directional light, 1.0f - point light
		XMFLOAT4 ambientLight;
		XMFLOAT4 diffuseLight;
		XMFLOAT4 specularLight;
		XMFLOAT4 emissionLight;
		FLOAT lightIntensity;
		XMFLOAT3 padding; // because of 16 byte alignement

		LightData(XMFLOAT3 lightPosition, LightType lightType) : ambientLight(0.0f, 0.0f, 0.0f, 0.0f), diffuseLight(0.0f, 0.0f, 0.0f, 0.0f), specularLight(0.0f, 0.0f, 0.0f, 0.0f), emissionLight(0.0f, 0.0f, 0.0f, 0.0f), lightIntensity(0.0f)
		{
			lightWorldPosition = { lightPosition.x, lightPosition.y, lightPosition.z, static_cast<FLOAT>(lightType) };

			/*switch (lightType)
			{
			case Light::Directional:
			default:
				lightWorldPosition.w = 0.0f;
				break;
			case Light::Point:
				lightWorldPosition.w = 1.0f;
				break;
			}*/
		}
	};

	INT init(ID3D11Device* pD3DDevice, LightData &light);
	void render(ID3D11DeviceContext* pD3DDeviceContext);
	void deInit();

private:
	ID3D11Buffer* _pLightBuffer = nullptr; // constant buffer with light data
};

